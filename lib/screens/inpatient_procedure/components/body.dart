import 'package:cost_of_care/bloc/inpatient_procedure_bloc/inpatient_procedure_bloc.dart';
import 'package:flutter/material.dart';
import '../../../models/outpatient_procedure.dart';
import 'inpatient_procedure_list_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<InpatientProcedureBloc, InpatientProcedureState>(
        builder: (BuildContext context, InpatientProcedureState state) {
      if (state is InpatientProcedureLoadingState) {
        return Center(
          child: Container(
            child: Center(child: CircularProgressIndicator()),
          ),
        );
      } else if (state is InpatientProcedureLoadedState) {
        List<OutpatientProcedure> inpatientProcedure =
            state.inpatientProcedures;
        return RefreshIndicator(
          onRefresh: () async {
            context
                .read<InpatientProcedureBloc>()
                .add(FetchData(isHardRefresh: true));
          },
          child: Scrollbar(
            child: ListView.builder(
              itemBuilder: (ctx, index) => makeCard(inpatientProcedure[index]),
              itemCount: inpatientProcedure.length,
            ),
          ),
        );
      } else if (state is InpatientProcedureErrorState) {
        return Center(
          child: Container(
            padding: EdgeInsets.all(8),
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).size.height / 3),
                Text(
                  state.message,
                  maxLines: 3,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    primary: Colors.white,
                    side: BorderSide(
                      color: Theme.of(context).primaryColor,
                      width: 1,
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                  ),
                  onPressed: () {
                    context.read<InpatientProcedureBloc>().add(FetchData());
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'RETRY',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
      return CircularProgressIndicator();
    });
  }

  @override
  void initState() {
    super.initState();
    context.read<InpatientProcedureBloc>().add(FetchData());
  }
}
